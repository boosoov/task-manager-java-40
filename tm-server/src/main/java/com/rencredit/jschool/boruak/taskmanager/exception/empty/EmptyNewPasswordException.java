package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyNewPasswordException extends AbstractException {

    public EmptyNewPasswordException() {
        super("Error! New password is empty...");
    }

}
