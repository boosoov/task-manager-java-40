package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTaskName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void createTaskNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    TaskDTO findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    TaskDTO findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    TaskDTO findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyUserIdException, EmptyNameException, DeniedAccessException;

    @NotNull
    @WebMethod
    TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, DeniedAccessException, EmptyDescriptionException;

    @NotNull
    @WebMethod
    TaskDTO updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, DeniedAccessException, EmptyDescriptionException;

    @Nullable
    @WebMethod
    void removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    void removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException, EmptyTaskException;

    @Nullable
    @WebMethod
    void removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void removeAllUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void clearAllTask(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyRoleException;

    @NotNull
    @WebMethod
    List<TaskDTO> getListTask(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException;

}
