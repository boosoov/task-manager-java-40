package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAbstractRepositoryEntity<E extends AbstractEntity> extends JpaRepository<E, String> {

}
