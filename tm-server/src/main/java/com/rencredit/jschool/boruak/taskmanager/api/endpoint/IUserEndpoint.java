package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @WebMethod
    void addUserLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    void addUserLoginPasswordFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    void addUserLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptySessionException;

    @Nullable
    @WebMethod
    UserDTO getUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @Nullable
    @WebMethod
    UserDTO getUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @Nullable
    @WebMethod
    UserDTO editUserProfileByIdFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) throws EmptyIdException, EmptyUserException, EmptyFirstNameException, DeniedAccessException;

    @Nullable
    @WebMethod
    UserDTO editUserProfileByIdFirstNameLastName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName
    ) throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException, DeniedAccessException;

    @Nullable
    @WebMethod
    UserDTO editUserProfileByIdFirstNameLastNameMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "email", partName = "email") String email,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException, DeniedAccessException;

    @Nullable
    @WebMethod
    UserDTO updateUserPasswordById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    ) throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException, DeniedAccessException;

    @WebMethod
    void removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    void removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

    @WebMethod
    void clearAllUser(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyUserException, EmptyLoginException, EmptyPasswordException, BusyLoginException, EmptyHashLineException;

    @NotNull
    @WebMethod
    List<UserDTO> getUserList(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException;

}
