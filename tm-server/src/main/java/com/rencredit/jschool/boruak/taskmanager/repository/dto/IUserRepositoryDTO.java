package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepositoryDTO extends IAbstractRepositoryDTO<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

}
