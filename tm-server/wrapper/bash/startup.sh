#!/usr/bin/env bash

PORT="8080"
if [ -n "$1" ]; then
  PORT=$1;
fi
FILE_PID="server-$PORT.pid"

echo "STARTING SERVER AT $PORT"
if [ -f ./$FILE_PID ]; then
	echo "Task-manager server already started"
	exit 1;
fi

mkdir -p ../bash/logs
rm -f ../bash/logs/tm-server.log
nohup java -Dport=$PORT -jar ../../tm-server.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > ./$FILE_PID
echo "TASK-MANAGER SERVER IS RUNNING WITH PID "$!
