package com.rencredit.jschool.boruak.taskmanager.dto;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class ProjectDTO extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    private String userId;

    public ProjectDTO() {
    }

    public ProjectDTO(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        this.userId = userId;
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

}
