package restendpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IProjectRestEndpointTest {

    IProjectRestEndpoint projectRestEndpoint;

    @Before
    public void init()  {
        projectRestEndpoint = IProjectRestEndpoint.client("http://localhost:8080/");
    }

    @After
    public void clearAll() {

    }

    @Test
    public void test() {
        final ProjectDTO project = new ProjectDTO();
        project.setDescription("111111");
        projectRestEndpoint.create(project);

        final ProjectDTO projectFromWeb = projectRestEndpoint.findOneByIdDTO(project.getId());
        Assert.assertEquals(project.getId(), projectFromWeb.getId());

        Assert.assertTrue(projectRestEndpoint.existsById(project.getId()));

        projectRestEndpoint.deleteOneById(project.getId());
        Assert.assertFalse(projectRestEndpoint.existsById(project.getId()));
    }

}
