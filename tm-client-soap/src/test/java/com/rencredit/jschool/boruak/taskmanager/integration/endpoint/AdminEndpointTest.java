package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.junit.experimental.categories.Category;

@Category(IntegrationWithServerTestCategory.class)
public class AdminEndpointTest {
//
//    @Autowired
//    @NotNull AdminEndpoint adminEndpoint;
//    @Autowired
//    @NotNull AuthEndpoint authEndpoint;
//    @Autowired
//    @NotNull UserEndpoint userEndpoint;
//    @Autowired
//    @NotNull ProjectEndpoint projectEndpoint;
//    @Autowired
//    @NotNull TaskEndpoint taskEndpoint;
//    @Autowired
//    @NotNull SessionEndpoint sessionEndpoint;
//    @NotNull SessionDTO session;
//
//    @Before
//    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, BusyLoginException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
//
//        session = sessionEndpoint.openSession("admin", "admin");
//    }
//
//    @After
//    public void clearAll() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyNameException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
//        userEndpoint.removeUserByLogin(session, "login1");
//        taskEndpoint.removeTaskByName(session, "name");
//        projectEndpoint.removeProjectByName(session,"name");
//        sessionEndpoint.closeSession(session);
//    }
//
//    @Test
//    public void testRoles() {
//        List<Role> role = adminEndpoint.roles();
//        Assert.assertEquals(Role.ADMIN, role.get(0));
//    }













//    @SneakyThrows
//    @Test
//    public void testSaveLoadClearBase64() {
//        userEndpoint.addUserLoginPassword(session,"login1", "password1");
//        taskEndpoint.createTaskName(session,"name");
//        projectEndpoint.createProjectName(session,"name");
//
//        boolean result = adminEndpoint.saveBase64(session);
//        Assert.assertTrue(result);
//
//        userEndpoint.removeUserByLogin(session, "login1");
//        taskEndpoint.removeTaskByName(session, "name");
//        projectEndpoint.removeProjectByName(session,"name");
//
//        Assert.assertNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.loadBase64(session);
//        Assert.assertTrue(result);
//
//        Assert.assertNotNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNotNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNotNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.clearBase64(session);
//        Assert.assertTrue(result);
//    }
//
//    @SneakyThrows
//    @Test
//    public void testSaveLoadClearBinary() {
//        userEndpoint.addUserLoginPassword(session,"login1", "password1");
//        taskEndpoint.createTaskName(session,"name");
//        projectEndpoint.createProjectName(session,"name");
//
//        boolean result = adminEndpoint.saveBinary(session);
//        Assert.assertTrue(result);
//
//        userEndpoint.removeUserByLogin(session, "login1");
//        taskEndpoint.removeTaskByName(session, "name");
//        projectEndpoint.removeProjectByName(session,"name");
//
//        Assert.assertNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.loadBinary(session);
//        Assert.assertTrue(result);
//
//        Assert.assertNotNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNotNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNotNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.clearBinary(session);
//        Assert.assertTrue(result);
//    }
//
//    @SneakyThrows
//    @Test
//    public void testSaveLoadClearJson() {
//        userEndpoint.addUserLoginPassword(session,"login1", "password1");
//        taskEndpoint.createTaskName(session,"name");
//        projectEndpoint.createProjectName(session,"name");
//
//        boolean result = adminEndpoint.saveJson(session);
//        Assert.assertTrue(result);
//
//        userEndpoint.removeUserByLogin(session, "login1");
//        taskEndpoint.removeTaskByName(session, "name");
//        projectEndpoint.removeProjectByName(session,"name");
//
//        Assert.assertNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.loadJson(session);
//        Assert.assertTrue(result);
//
//        Assert.assertNotNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNotNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNotNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.cleanJson(session);
//        Assert.assertTrue(result);
//    }
//
//    @SneakyThrows
//    @Test
//    public void testSaveLoadClearXml() {
//        userEndpoint.addUserLoginPassword(session,"login1", "password1");
//        taskEndpoint.createTaskName(session,"name");
//        projectEndpoint.createProjectName(session,"name");
//
//        boolean result = adminEndpoint.saveXml(session);
//        Assert.assertTrue(result);
//
//        userEndpoint.removeUserByLogin(session, "login1");
//        taskEndpoint.removeTaskByName(session, "name");
//        projectEndpoint.removeProjectByName(session,"name");
//
//        Assert.assertNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.loadXml(session);
//        Assert.assertTrue(result);
//
//        Assert.assertNotNull(userEndpoint.getUserByLogin(session,"login1"));
//        Assert.assertNotNull(taskEndpoint.findTaskByName(session, "name"));
//        Assert.assertNotNull(projectEndpoint.findProjectByName(session, "name"));
//
//
//        result = adminEndpoint.clearXml(session);
//        Assert.assertTrue(result);
//    }

}
