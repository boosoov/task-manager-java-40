package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class InfoServiceTest {
//
//    @NotNull private IServiceLocator serviceLocator;
//    @NotNull private IInfoService infoService;
//
//    @Before
//    public void init() {
//        serviceLocator = new ServiceLocator();
//        infoService = new InfoService(serviceLocator);
//    }
//
//    @Test
//    public void testGetArgumentList() {
//        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
//        @NotNull final String[] arguments = commandService.getArgs();
//        @NotNull final StringBuilder resultString = new StringBuilder();
//        for (@NotNull final String argument : arguments) {
//            resultString.append(argument).append("\n");
//        }
//        @NotNull final String argumentList = resultString.toString();
//
//        Assert.assertEquals(argumentList, infoService.getArgumentList());
//    }
//
//    @Test
//    public void testGetCommandList() {
//        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
//        @NotNull final String[] commands = commandService.getCommands();
//        @NotNull final StringBuilder resultString = new StringBuilder();
//        for (@NotNull final String command : commands) {
//            resultString.append(command).append("\n");
//        }
//        @NotNull final String commandList = resultString.toString();
//
//        Assert.assertEquals(commandList, infoService.getCommandList());
//    }
//
//    @Test
//    public void testGetDeveloperInfo() {
//        @NotNull final String developerInfo = "NAME: Boruak Sergey\nE-MAIL: boosoov@gmail.com\n";
//        Assert.assertEquals(developerInfo, infoService.getDeveloperInfo());
//    }
//
//    @Test
//    public void testGetHelp() throws EmptyCommandException {
//        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
//        @NotNull final Map<String, AbstractCommand> commands = commandService.getTerminalCommands();
//        @NotNull final StringBuilder resultString = new StringBuilder();
//        for (@NotNull final Map.Entry<String, AbstractCommand> command : commands.entrySet()) {
//            @NotNull final AbstractCommand commandValue = command.getValue();
//            if (commandValue == null) throw new EmptyCommandException();
//            if (!commandValue.name().isEmpty()) resultString.append(commandValue.name());
//            @Nullable final String arg = commandValue.arg();
//            if (arg != null && !arg.isEmpty()) resultString.append(", ").append(commandValue.arg());
//            if (!commandValue.description().isEmpty()) resultString.append(": ").append(commandValue.description());
//            resultString.append("\n");
//        }
//        @NotNull final String help = resultString.toString();
//
//        Assert.assertEquals(help, infoService.getHelp());
//    }
//
//    @Test
//    public void testGetSystemInfo() {
//        @NotNull final StringBuilder resultString = new StringBuilder();
//        @NotNull final int processors = Runtime.getRuntime().availableProcessors();
//        resultString.append("Available processors: ").append(processors).append("\n");
//        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
//        resultString.append("Free memory: ").append(NumberUtil.formatBytes(freeMemory)).append("\n");
//        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
//        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
//        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
//        resultString.append("Maximum memory: ").append(maxMemoryValue).append("\n");
//        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
//        resultString.append("Total memory available to JVM: ").append(NumberUtil.formatBytes(totalMemory)).append("\n");
//        @NotNull final long usedMemory = totalMemory - freeMemory;
//        resultString.append("Used memory by JVM: ").append(NumberUtil.formatBytes(usedMemory)).append("\n");
//        @NotNull final String systemInfo = resultString.toString();
//
//        Assert.assertEquals(systemInfo, infoService.getSystemInfo());
//    }
//
//    @Test
//    public void testGetVersion() {
//        @NotNull final String version = "1.0.0\n";
//        Assert.assertEquals(version, infoService.getVersion());
//    }

}
