package com.rencredit.jschool.boruak.taskmanager.exception;

public class AbstractClientException extends Exception {

    public AbstractClientException() {}

    public AbstractClientException(final String message) {
        super(message);
    }

    public AbstractClientException(final Throwable cause) {
        super(cause);
    }

}
