package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class NotExistProjectsListException extends AbstractClientException {

    public NotExistProjectsListException() {
        super("Error! Project list is not exist...");
    }

}
