package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DeveloperInfoShowListener extends AbstractListener {

    @Autowired
    private InfoService infoService;

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    @EventListener(condition = "@developerInfoShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event){
        System.out.println("[DEVELOPER INFO]");
        System.out.println(infoService.getDeveloperInfo());
    }

}
