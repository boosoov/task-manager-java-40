package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class SystemInfoListener extends AbstractListener {

    @Autowired
    private InfoService infoService;

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show computer info.";
    }

    @Override
    @EventListener(condition = "@systemInfoListener.name() == #event.command")
    public void handle(final ConsoleEvent event){
        System.out.println("[SYSTEM INFO]");
        System.out.println(infoService.getSystemInfo());
    }

}
