package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyNameException extends AbstractClientException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
