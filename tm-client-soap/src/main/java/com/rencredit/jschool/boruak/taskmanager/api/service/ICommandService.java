package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public interface ICommandService {

    @NotNull
    Map<String, AbstractListener> getTerminalCommands();

    @NotNull
    String[] getCommands();

    @NotNull
    String[] getArgs();

    void putCommand(@Nullable final String name, @Nullable final AbstractListener command) throws EmptyCommandException, EmptyNameException;

}
