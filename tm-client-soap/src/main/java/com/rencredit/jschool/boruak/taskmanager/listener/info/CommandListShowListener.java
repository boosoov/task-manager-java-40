package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class CommandListShowListener extends AbstractListener {

    @Autowired
    private InfoService infoService;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    @EventListener(condition = "@commandListShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event){
        System.out.println("[COMMANDS]");
        System.out.println(infoService.getCommandList());
    }

}
