package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class NotExistTasksListException extends AbstractClientException {

    public NotExistTasksListException() {
        super("Error! Task list is not exist...");
    }

}
