package com.rencredit.jschool.boruak.taskmanager.exception.unknown;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class UnknownCommandException extends AbstractClientException {

    public UnknownCommandException() {
        super("Error! Unknown command...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Unknown command ``" + command + "``...");
    }

}
