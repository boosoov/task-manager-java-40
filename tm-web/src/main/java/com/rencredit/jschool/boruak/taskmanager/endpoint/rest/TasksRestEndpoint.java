package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITasksRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping
    public List<TaskDTO> getListDTO() {
        return taskService.getListDTO();
    }

    @Override
    @PostMapping
    public List<TaskDTO> saveAll(@RequestBody List<TaskDTO> list) {
        return taskService.saveAll(list);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        taskService.deleteAll();
    }

    @Override
    @DeleteMapping
    public void deleteAll(@RequestBody List<TaskDTO> list) {
        taskService.deleteAll(list);
    }

}
