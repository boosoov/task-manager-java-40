package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/TaskEndpoint?wsdl
 */

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;

    @WebMethod
    public void createTask(
            @WebParam(name = "task") TaskDTO task
    ) throws EmptyTaskException, EmptyUserIdException, EmptyLoginException {
        taskService.create(task);
    }

    @WebMethod
    public TaskDTO findTaskByIdDTO(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        return taskService.findOneByIdDTO(user.getId(), id);
    }

    @WebMethod
    public void deleteTask(
            @WebParam(name = "task") TaskDTO task
    ) throws EmptyUserIdException, EmptyLoginException, EmptyTaskException {
        taskService.delete(task);
    }

    @WebMethod
    public void deleteTaskById(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        taskService.deleteOneById(user.getId(), id);
    }

    @WebMethod
    public List<TaskDTO> getListTasks() {
        return taskService.getListDTO();
    }

    @WebMethod
    public void deleteAllTasks() {
        taskService.deleteAll();
    }

    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "id") String id
    ) {
        return taskService.existsById(id);
    }

}
