package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;

@Component
public class Bootstrap {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ISessionService sessionService;

    public Bootstrap() {
    }

    public void init() {
        try {
            initUsers();
        } catch (EmptyHashLineException e) {
            e.printStackTrace();
        } catch (BusyLoginException e) {
            e.printStackTrace();
        } catch (EmptyPasswordException e) {
            e.printStackTrace();
        } catch (EmptyLoginException e) {
            e.printStackTrace();
        } catch (EmptyUserException e) {
            e.printStackTrace();
        } catch (DeniedAccessException e) {
            e.printStackTrace();
        } catch (EmptyRoleException e) {
            e.printStackTrace();
        }
        sessionService.closeAll();
    }

    private void initUsers() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {
        userService.addUser("1", "1");
        userService.addUser("test", "test");
        userService.addUser("admin", "admin", Role.ADMIN);
    }

}
