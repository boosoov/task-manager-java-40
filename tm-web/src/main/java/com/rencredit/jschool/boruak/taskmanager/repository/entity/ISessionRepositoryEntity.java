package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.springframework.stereotype.Repository;

@Repository
public interface ISessionRepositoryEntity extends IAbstractRepositoryEntity<Session> {

}
