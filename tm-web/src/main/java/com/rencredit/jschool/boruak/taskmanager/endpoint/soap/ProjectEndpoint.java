package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;

import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/ProjectEndpoint?wsdl
 */

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @WebMethod
    public void createProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.create(user.getId(), project);
    }

    @WebMethod
    public ProjectDTO findProjectByIdDTO(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        return projectService.findOneByIdDTO(user.getId(), id);
    }

    @WebMethod
    public void deleteProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.delete(user.getId(), project);
    }

    @WebMethod
    public void deleteProjectById(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.deleteOneById(user.getId(), id);
    }

    @WebMethod
    public List<ProjectDTO> getListProjects() {
        return projectService.getList();
    }

    @WebMethod
    public void deleteAllProjects() {
        projectService.deleteAll();
    }

    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "id") String id
    ) {
        return projectService.existsById(id);
    }

}
