package com.rencredit.jschool.boruak.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDataBasePropertyService {

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcUsername();

    @NotNull
    String getJdbcPassword();

}
