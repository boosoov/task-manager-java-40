package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class NotExistTasksListException extends AbstractException {

    public NotExistTasksListException() {
        super("Error! Task list is not exist...");
    }

}
