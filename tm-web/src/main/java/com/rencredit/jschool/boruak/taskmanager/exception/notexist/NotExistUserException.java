package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class NotExistUserException extends AbstractException {

    public NotExistUserException() {
        super("Error! User is not exist...");
    }

}
