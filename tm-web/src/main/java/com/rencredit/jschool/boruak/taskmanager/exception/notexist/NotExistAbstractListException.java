package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class NotExistAbstractListException extends AbstractException {

    public NotExistAbstractListException() {
        super("Error! Abstract list is not exist...");
    }

}
